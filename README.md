# How to install remix os on linux system
###  Install Remix OS on Linux System without partitioning 

  * Download REMIX iso
  * Make sure you're using linux system (I'n my case i'm using arch linux)

  1. At first create those folder inside your root.
   ```
   /remix
   /remix/data
   
   Example : 
   [farhan@sadik]$ sudo mkdir /remix
   [farhan@sadik]$ sudo mkdir /remix/data
   ```
  2. Extract the phoenix iso. 
  ```
  Example : 
  [farhan@sadik]$ 7z x -y PhoenixOSInstaller_v3.0.8.529_x86_x64.iso
  ```
  3. After extracting delete the `PhoenixOSInstaller_v3.0.8.529_x86_x64.iso` file from your current folder.
  4. Extract `system.sfs` file.
  ```
  Example : 
  [farhan@sadik]$ unsquashfs system.sfs
  [farhan@sadik]$ rm -rv system.sfs
  ```
  5. After compleating this process, please delete `system.sfs` file from your current folder.
  5. Open `squashfs-root` folder and then copy `system.img` folder to current directory. And then delete `squashfs-root` folder.
  6. Copy all of files into `/phoenix` folder. 
  The selection (which file you've to copy) file is 
  ```
  [farhan@sadik]$ ls 
  efi         install.img  kernel       system.img
  initrd.img  isolinux     ramdisk.img  TRANS.TBL

  Example :
  [farhan@sadik]$ sudo cp -rv * /phoenix/
  ```
  7. Install grub-customizer for updating grub. Or you can do it manually by terminal. 
  ```
  Example :
  # For Arch Linux and arch based distros 
  [farhan@sadik]$ sudo pacman -S grub-customizer

  # For Debian and Debian based distros
  $ sudo add-apt-repository ppa:danielrichter2007/grub-customizer
  $ sudo apt-get update
  $ sudo apt-get install grub-customizer
  ```
  8. Open `grub-customizer` and then create a new entry. Then `name` it to "`Phoenix OS`" and select type as `other`
  9. Before adding data check your file system where you've installed your linux system. Check it by, `sudo fdisk -l` command.
  Output looks like this : 
  ```
  farhan@sadik]$ sudo fdisk -l
  [sudo] password for farhansadik: 
  Disk /dev/sda: 465.8 GiB, 500107862016 bytes, 976773168 sectors
  Units: sectors of 1 * 512 = 512 bytes
  Sector size (logical/physical): 512 bytes / 4096 bytes
  I/O size (minimum/optimal): 4096 bytes / 4096 bytes
  Disklabel type: dos
  Disk identifier: 0x2dfaae56

  Device     Boot     Start       End   Sectors   Size Id Type
  /dev/sda1            2048 102398309 102396262  48.8G  7 HPFS/NTFS/exFAT
  /dev/sda2       102398371 976768043 874369673   417G  f W95 Ext'd (LBA)
  /dev/sda5       102398373 321396389 218998017 104.4G  7 HPFS/NTFS/exFAT
  /dev/sda6       321396736 476585983 155189248    74G  7 HPFS/NTFS/exFAT
  /dev/sda7       540394533 976768043 436373511 208.1G  7 HPFS/NTFS/exFAT
  /dev/sda8       476588032 540392484  63804453  30.4G 83 Linux

  Partition 2 does not start on physical sector boundary.
  Partition 5 does not start on physical sector boundary.
  Partition 7 does not start on physical sector boundary.
  Partition table entries are not in disk order.

  ```
  in my case my `linux root` system is `/dev/sda8` check it yours. Which is mean the value is `8`. <br>
  10. Enter those data inside the box
  > Note : enter your hdd value on 2nd line which is `set root='hd0,<your value>'`
  ```
    insmod part_gpt 
    set root='hd0,2' 
    linux /remix/kernel root=/dev/ram0 androidboot.hardware=remix_x86_64 androidboot.selinux=permissive quiet SERIAL=random logo.showlogo=1 SRC= DATA= CREATE_DATA_IMG=1
    initrd /remix/initrd.img

  ```
  11. Save it. And Rebot.
  12. I HOPE EVERYTHING WORKS FINE
  
## Written By,
Farhan Sadik
